UI Login Page

Use ReactJS to create the front end of the login page

## Screenshots: 

![] (https://colorlib.com/wp/wp-content/uploads/sites/2/login-form-v1.jpg.webp)
_samples on request_

![](./images/images126.png)
_Result_

![](./images/images125.png)
_email điền chưa đúng hiển thị Email incorrect format!_

![](./images/images125.png)
_thiếu email hiển thị This is the required!_

![](./images/images127.png)
_nhap password xoa di se hien thi Pass is required_

![](./images/images124.png)
_đăng nập thành công hiển thị tên email và Logout để về trang chủ_

## Installation
1. Clone project:
```
git clone https://gitlab.com/tranxuantuyen2512/reactjs.git
```
2. Install required dependencies:
```
npm install
```
3. Run app:
```
npm start
```

## References:
https://www.youtube.com/watch?v=91qEdc6dSUs
