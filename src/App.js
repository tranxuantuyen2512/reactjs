import React, {useState} from 'react';
import LoginForm from './component/LoginForm';


function App() {
 
  
  const [user, setUser] = useState({email: ""});

  const handleUpdateEmail = (info) => {
    setUser({email: info});
  }

  const logout = () => {
    setUser({email: ""});
  }
  
  return (
    <div className="handleLogin">
      {(user.email) ? (
        <div className='welcome'>
          <h2>{user.email}</h2>
          <h2>Đăng nhập thành công</h2>
          <button onClick={logout}>LOGOUT</button>
        </div>
      ) : (
        < LoginForm  handleChangeEmail={ (email) => handleUpdateEmail(email)}/>
      )}
    </div>
  );
}

export default App   ;
