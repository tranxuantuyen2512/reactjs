/* eslint-disable jsx-a11y/anchor-is-valid */
import { ArrowRightAlt } from '@mui/icons-material';
import React, { useState } from 'react';
import { regexEmail } from '../constants';




function LoginForm({handleChangeEmail}) {

  const [infoUser, setInfoUser] = useState({ email: "", password: "" });

  const [errorEmail, setErrorEmail] = useState('');
  const [errorPassword, setErrorPassword] = useState('');


  const changeEmail = (e) => {
    const { value } = e.target;

    if (value?.trim() === '') {
      setErrorEmail('This is required!');
    } else if (!regexEmail.test(value)) {
      setErrorEmail('Email incorrect format!');
    } else {
      setErrorEmail('');
    }

    setInfoUser({ ...infoUser, email: value })
  }

  const changePassword = (e) => {
    const { value } = e.target;

    if (value?.trim() === '') {
      setErrorPassword("Pass is required");
    } else {
      setErrorPassword('')
    }
    setInfoUser({ ...infoUser, password: e.target.value })
  }


  const handleSubmit = e => {
    e.preventDefault();

    if (infoUser.email === '') {
      setErrorEmail('This is required!');
    } else {
      setErrorEmail('');
    }

    if (infoUser.password === '') {
      setErrorPassword('Password is required!');
    } else {
      setErrorPassword('');
    }
    handleChangeEmail(infoUser.email) 
    // truyen du lieu email ra App
  }

  return (
    <form onSubmit={handleSubmit}>
      <div className='form-inner'>
        <div className='flex1'>,<a href=""><img src='https://static.vecteezy.com/system/resources/previews/000/424/117/original/computer-icon-vector-illustration.jpg' alt="anhdep" width="180px" /></a></div>
        <div className='flex2'>
          <div className='form-content'>
            <div><h2>Member Login</h2></div>

            <div className='form-group'>
              <i className="fa-solid fa-envelope"></i><input className={errorEmail ? 'input-error' : ''} type="email" name='email' id="email" placeholder="Email" onChange={e => changeEmail(e)} value={infoUser.email} />
            </div>
            {errorEmail && <p className='error'>{errorEmail}</p>}

            <div className='form-group'>
              <i className="fa-solid fa-key"></i><input className={errorPassword ? 'input-error' : ''} type="password" name="password" id="password" placeholder="Password" onChange={e => changePassword(e)} value={infoUser.password} />
              {errorPassword && <p className='error'>{errorPassword}</p>}
            </div>
            <div className='form2'><input type="submit" value="LOGIN" /></div>
            <div className='form3'><a href="">Forgot Username </a>/<a href=''> Password?</a></div>



          </div>

          <div className='form4'>
            <a href='' alt="">Create your Account </a><a href=''><ArrowRightAlt /></a>
          </div>


        </div>
      </div>
    </form>
  )
}

export default LoginForm;